import * as pulumi from "@pulumi/pulumi";
import * as azure_native from "@pulumi/azure-native";
import * as resources from "@pulumi/azure-native/resources";

// Create an Azure Resource Group
const resourceGroup = new resources.ResourceGroup("ayamachi");

const staticSite = new azure_native.web.StaticSite("corporate-site", {
    branch: "main",
    buildProperties: {
        apiLocation: "api",
        appArtifactLocation: "build",
        appLocation: "app",
    },
    location: "eastasia",
    name: "corporate-site",
    resourceGroupName: resourceGroup.name,
    sku: {
        tier: "Standard",
        name: "Standard",
    }
});

const staticSiteCustomDomain = new azure_native.web.StaticSiteCustomDomain("ayamachi-co-jp", {
    domainName: "ayamachi.co.jp",
    name: staticSite.name,
    resourceGroupName: resourceGroup.name,
    validationMethod: "dns-txt-token"
});

