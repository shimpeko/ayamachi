export default function Head() {
  return (
    <>
      <title>株式会社あやまち</title>
      <meta content="width=device-width, initial-scale=1" name="viewport" />
      <meta name="description" content="株式会社あやまち" />
    </>
  )
}
