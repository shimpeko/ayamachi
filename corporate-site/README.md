# あやまち株式会社

あやまち株式会社's corporate website.

## Develop

### Initial Setup

#### Install NodeJS

If you haven't installed node and npm it is recommended to install them with a version manager like `nvm`. See https://github.com/nvm-sh/nvm#installing-and-updating to install them by `nvm`. Run `nvm use` after installing the `nvm` to pick the node version for this repository which is defined in `./.nvmrc`.

You can check the existing installation by running the following command;

```
node --version
npm --version
```

Ccheck https://docs.npmjs.com/downloading-and-installing-node-js-and-npm for more about node installation.

#### Install modules

```
npm install
```

### Run HTTP server locally

```
npx run dev
```

## Deploy

### Auto Deploy

The CI job automatically deploys the website.

### Manual Deploy

Run the following command where `$DEPLOYMENT_TOKEN` is the deployment token for Azure Static Web App.

```
npx swa deploy -d $DEPLOYMENT_TOKEN
```

